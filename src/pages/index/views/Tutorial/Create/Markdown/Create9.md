!!!!

Choose a unique key for your project template. Choose a completely unique key that **no one has used** if you wish to 
publish your template. You can check if the name has been taken by searching on the search bar above. 

The key has to fulfil the following requirements:
- Has to start with a lower-cased alphabet
- Only contains lower-cased alphanumeric and "_"
- Has to be unique (if you want to publish online). Recommended to prepend your username with underscore in front. 
