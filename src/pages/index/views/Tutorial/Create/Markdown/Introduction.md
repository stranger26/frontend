# Introduction

CyanPrint template works on extremely simple concepts. If you are familiar with 
[CookieCutter](https://github.com/audreyr/cookiecutter), the concept was inspired by
CookieCutter.

The most basic CyanPrint functionality is to replace variable detected in your 
**file and folder**'s  **content and name**, and replaces them with what the 
user selected or inputted.

!!!!

More advance feature include:
- comment flags
- if-else resolution
- GUID generation
- NPM Package flagging
- Auto Git initialization
- Open Source project scaffolding (README.MD, LICENSE.MD, CONTRIBUTING.MD)
- Running of shell command during, before and after template generation
- Permute options to check possible outcomes of templates

!!!! 

Once you have installed you can move on to our next section to create your first CyanPrint!

!!!!
