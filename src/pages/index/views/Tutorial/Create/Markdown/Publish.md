# Publishing your Template

This tutorial will explain how to publish your newly created template!

!!!!

1. **Create a Personal Access Token**

   To publish your template, you need a personal access token to authenticate against the CyanPrint server.
   
   To create a personal access token, click [here](/profile/token). After that, click `Create Token` button, follow 
   the instructions and store your token in a safe place.
    
2. **Push the template to remote**
    
    At the root of the template folder (the one containing `cyanprint.js` and `cyan.json`, run the push command:
    
    <div class='b-flex'>
    
    ```bash
    $ cyan push <token>
    ```
    
    or 
    
    ```bash 
    $ cyan p <token>   
    ```
    </div>
    
    where `<token>` is your personal access token
    
!!!!

You have successfully pushed your template to the remote repository!

!!!!

You should be able to find your template in the search bar above!

!!!!

Next, you can see how to integrate automatic publishing with GitLab CI!
