!!!!

After choosing from the list of templates you have installed, it will run the
template's configuration questions.

> For checkbox-type selection, press 'space-bar' on your keyboard to check or uncheck and option

> Press 'Enter' after you are satisfied with your selections to move on to the next step
