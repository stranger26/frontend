# Introductions
Group are a collection of templates. Grouping them comes with 2 benefits

1. When updating, you can update a group as a whole
2. You can publish your group, using a set of templates you use often across machines

In this tutorial, you will learn how to:

1. Create an empty group
2. Populate your group
3. Install online groups
4. Publish your group 

Updating groups will be covered in another tutorial, but for convenience, a link will be left here!
 
# Creating a Group

A sample command of creating a group:
```bash
$ cyan group create kirinnee_my_first_group "My First Group" kirinnee97@gmail.com 
```
The command follows the syntax:

```bash
$ cyan group create <unique_key> "<display_name>" <author_email>
```
> ProTip `group` has a shorthand of `g` and `create` has a shorthand of `c`

!!!!

There are restrictions to each value:

**Unique Key**
- Has to start with a lower-cased alphabet
- Only contains lower-cased alphanumeric and "_"
- Has to be unique (if you want to publish online). Recommended to prepend your username with underscore in front. 
Eg: `<name>_<key>`

**Display Name**
- Has to start with an alphabet. Casing does not matter.
- Can only contain alphanumeric, "_", " " and "-" characters. Casing does not matter.
- Does not have to be unique

**Author Email**
- Has to the email tied to your CyanPrint account (if you want to publish online)

!!!!

With the restrictions and information, create your very first group:

```bash
cyan g c <your_name>_my_first_group "My First Group" <your_email>
```
*Replace `<your_name>` and `<your_email>` with actual values!*

!!!!

Your should see :
```
The Group <your_name>_my_first_group (My First Group) has been created!
```

!!!!

Congratulations! You have created your first group!


# Installing templates to a group

To install a template your group, just add the `group unique key` to the end of your `cyan i` command!

> By default, if you do not provide a group, it will install to the `main` group, whose name is `Built-In Template`

!!!!



Example:
*This example assumes you have <your_name>_my_first_group created*
```bash
$ cyan i vue2 <your_name>_my_first_group
```

!!!!

You have installed vue2 into your group!

!!!!

To try out the group: 

1. Create a template `cyan c app    `
2. Select the group you created

!!!!

You should see the vue2 template in the group!

!!!!

With this you can populate groups with templates you like! 


# Install a remote group
There are groups created by others, or even yourself, published to the CyanPrint repository.
CyanPrint provides you the ability to install a group (and all its template) into your machine
with a single command.

!!!!

The following example shows how to install the online group with the unique key `node`, a group that
contains template around the Javascript ecosystem : 

<div class='b-flex'>

```bash
$  cyan group install frontend
```

or 

```bash
$ cyan g i frontend
```
</div>

> ProTop: `group` has a shorthand `g` and `install` have a shorthand `i`

!!!!

Now when you fire up `cyan create app`, you should see the new group `Frontend` and all the template it contains

# Publish your group

For use your group in the other machines or share your groups with others, you can publish you group to the 
CyanPrint repository. This functionality comes in-built from the CyanPrint CLI.

!!!!

1. **Create a Personal Access Token**

   To publish your very own group, you need a personal access token to authenticate against the CyanPrint server.
   
   To create a personal access token, click [here](/profile/token). After that, click `Create Token` button, follow 
   the instructions and store your token in a safe place.
2. **Create a group**
    
    Follow the instructions at [Create a Group](#creating-a-group) to create a group.
    
3. **Push the group to remote**
    
    Run the push command:
    
    <div class='b-flex'>
    
    ```bash
    $ cyan group push <key> <token>
    ```
    
    or 
    
    ```bash 
    $ cyan g p <key> <token>   
    ```
    </div>
    
    where `<key>` is the **Unique Key** of the group and `<token>` is your personal access token
    
!!!!

You have successfully pushed your group to the remote repository!

> You should be able search for your template with the search bar above

# Update a group

There are 2 different methods of updating a group:

!!!!

1. Update the group from remote
    
    This pulls the group data from CyanPrint and update the group according to the new data from remote
    
    For more information read the [tutorial on how to update a remote group](/tutorial/use/update#update-a-remote-group)
    
2. Update group locally
    
    This method simply updates every template to the latest version within itself
    For more information read the [tutorial on how to update a local group](/tutorial/use/update#update-a-local-group)
    
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!

    
 
