import {Core, Kore} from "@kirinnee/core";
import {WebAuth} from "auth0-js";
import {AuthService} from "../../classLibrary/AuthService";
import authConfig from "../../../config/auth_config.json";
import {CyanPrintSDK} from "../../classLibrary/CyanPrintSDK";
import {DOMEx, DOMExtend} from "@kirinnee/domex";
import {EleFact, ElementFactory} from "@kirinnee/elefact";
import * as gsap from 'gsap';
import {TweenLite} from 'gsap';
import {EaseFactory, kEaseFactory} from "@kirinnee/kease";
import {AsynchronousAnimator, GSAPAsyncAnimator, GSAPSyncAnimator, SynchronousAnimator} from "@kirinnee/animate";
import {AnimateX, AnimX} from "@kirinnee/animatex";

const text = require('gsap/TextPlugin');
const core: Core = new Kore();
core.ExtendPrimitives();

const domex: DOMEx = new DOMExtend(core);
domex.ExtendPrimitives();

const eases: EaseFactory = new kEaseFactory(gsap);
const eleFact: ElementFactory = new EleFact(domex, "k-space");

//Construct Synchronous animator
const animator: SynchronousAnimator = new GSAPSyncAnimator(TweenLite, text, eases, eleFact, domex, core);

//Construct Asynchronous animator
const asyncAnimator: AsynchronousAnimator = new GSAPAsyncAnimator(animator);

//Extend the animator
const animX: AnimateX = new AnimX(asyncAnimator);
animX.ExtendPrimitives();


declare var PRODUCTION: boolean;

const config: { domain: string, clientId: string } = PRODUCTION ? authConfig.prod : authConfig.dev;

const webAuth: WebAuth = new WebAuth({
	domain: config.domain,
	redirectUri: `${window.location.origin}/callback`,
	clientID: config.clientId,
	responseType: 'id_token',
	scope: 'openid email profile'
});
const auth: AuthService = new AuthService(webAuth);
const host: string = PRODUCTION ? "https://api.cyanprint.dev" : "http://localhost:3001";
const sdk: CyanPrintSDK = new CyanPrintSDK(host, auth);

if (!PRODUCTION) {
	const w: any = window;
	w.auth = auth;
	w.sdk = sdk;
}

const $$ = (v: number) => new Promise<void>(r => setTimeout(r, v));

const MonthParse = (month: number): string => {
	switch (month) {
		case 0 :
			return "January";
		case 1:
			return "February";
		case 2:
			return "March";
		case 3:
			return "April";
		case 4:
			return "May";
		case 5:
			return "June";
		case 6:
			return "July";
		case 7:
			return "August";
		case 8:
			return "September";
		case 9:
			return "October";
		case 10:
			return "November";
		case 11:
			return "December";
	}
	return "";
};

export {
	core,
	auth,
	sdk,
	eleFact,
	eases,
	$$,
	MonthParse,
}
