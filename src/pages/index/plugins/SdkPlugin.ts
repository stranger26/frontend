import {sdk} from "../init";
import {CyanPrintSDK} from "../../../classLibrary/CyanPrintSDK";

declare module "vue/types/vue" {
	interface Vue {
		$api: CyanPrintSDK;
	}
}

export default {
	install(Vue: any) {
		Vue.prototype.$api = sdk;
		
		Vue.mixin({
			created() {
				if (this.handleApiError) {
					sdk.addListener(sdk.eventKey, this.handleApiError);
				}
			},
			
			destroyed() {
				if (this.handleApiError) {
					sdk.removeListener(sdk.eventKey, this.handleApiError);
				}
			}
		});
	}
	
	
};
