import Vue from 'vue'
import Router, {Route, RouteConfig} from 'vue-router'
import Callback from "./components/Callback.vue";
import Profile from "./views/Profile/Profile.vue";
import Home from "./views/Home/Home.vue";
import About from "./views/About/About.vue";
import {auth} from "./init";
import NotFound from "./views/NotFound.vue";
import VerifyEmail from "./views/VerifyEmail.vue";
import Search from "./views/Search/Search.vue";
import TemplateView from "./views/Template/TemplateView.vue";
import GroupView from "./views/Group/GroupView.vue";
import TokenView from "./views/Profile/TokenView.vue";
import ProfileTemplate from "./views/Profile/ProfileTemplate.vue";
import ProfileGroup from "./views/Profile/ProfileGroup.vue";
import CreateToken from "./views/CreateToken.vue";
import GettingStarted from "./views/Tutorial/GettingStarted.vue";
import UseStart from "./views/Tutorial/Usage/UseStart.vue";
import GroupUsage from "./views/Tutorial/Usage/GroupUsage.vue";
import UpdateUsage from "./views/Tutorial/Usage/UpdateUsage.vue";
import Installation from "./views/Tutorial/Create/Installation.vue";
import Introduction from "./views/Tutorial/Create/Introduction.vue";
import Scaffold from "./views/Tutorial/Create/Scaffold.vue";
import GetStarted from "./views/Tutorial/Create/GetStarted.vue";
import Write from "./views/Tutorial/Create/Write.vue";
import InquireInput from "./views/Tutorial/Create/InquireInput.vue";
import PublishTemplate from "./views/Tutorial/Create/PublishTemplate.vue";
import AutoGitLab from "./views/Tutorial/Create/AutoGitLab.vue";
import DocumentationMain from "./views/Documentation/DocumentationMain.vue";

Vue.use(Router);

const meta = {public: true, private: true};

const routes: RouteConfig[] = [
	{path: '/', name: 'home', component: Home, meta},
	{path: '/callback', name: 'callback', component: Callback, meta},
	{
		path: '/profile', name: 'profile', component: Profile, meta: {public: false, private: true},
		children: [
			{path: 'token', name: 'profile-token', component: TokenView, meta: {public: false, private: true}},
			{path: 'template', name: 'profile-temp', component: ProfileTemplate, meta: {public: false, private: true}},
			{path: 'group', name: 'profile-group', component: ProfileGroup, meta: {public: false, private: true}}
		]
	},
	{
		path: '/docs', name: 'docs', component: DocumentationMain, meta,
	},
	{
		path: '/tutorial', name: 'tutorial', component: GettingStarted, meta,
		children: [
			{path: 'use/start', name: 'use-start', component: UseStart, meta},
			{path: 'use/groups', name: 'use-group', component: GroupUsage, meta},
			{path: 'use/update', name: 'use-update', component: UpdateUsage, meta},
			{path: 'create/start', name: 'create-start', component: GetStarted, meta},
			{path: 'create/prerequisite', name: 'create-prerequisite', component: Installation, meta},
			{path: 'create/introduction', name: 'create-introduction', component: Introduction, meta},
			{path: 'create/scaffold', name: 'create-scaffold', component: Scaffold, meta},
			{path: 'create/write', name: 'create-write', component: Write, meta},
			{path: 'create/input', name: 'create-input', component: InquireInput, meta},
			{path: 'create/publish', name: 'create-publish', component: PublishTemplate, meta},
			{path: 'create/gitlab', name: 'create-gitlab', component: AutoGitLab, meta},
		]
	},
	{path: '/token/create', name: 'create-token', component: CreateToken, meta: {public: false, private: true}},
	{path: '/about', name: 'about', component: About, meta},
	{path: '/search/:type/:id', name: 'search', component: Search, meta},
	{path: '/search/:type/', name: 'search', component: Search, meta},
	{path: '/template/:id', name: 'template', component: TemplateView, meta},
	{path: '/group/:id', name: 'group', component: GroupView, meta},
	{path: '*', name: '404', component: NotFound, meta},
	{path: '/verify_email', name: 'verify_email', component: VerifyEmail, meta: {public: false, private: true}}
];
const router: Router = new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes
});

router.beforeEach(async (to: Route, from: Route, next) => {
	const guard: { public: boolean, private: boolean } = to.meta;
	if (to.path === "/callback") return next();
	if (guard.public && !auth.isAuthenticated()) return next();
	if (guard.private && auth.isAuthenticated()) {
		if (!guard.public && !auth.profile!.email_verified && to.path !== "/verify_email") {
			router.push("/verify_email");
			return;
		} else {
			return next();
		}
	}
	auth.login({target: to.path});
});
export {router};
