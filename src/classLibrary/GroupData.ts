interface GroupDataLiteral {
	display_name: string;
	unique_key: string;
	downloads: number;
	stars: number;
	readme: string;
	templates: string[];
	author: string;
}

class GroupData {
	literal: GroupDataLiteral;
	
	constructor(literal: GroupDataLiteral) {
		this.literal = literal;
	}
}

export {GroupData, GroupDataLiteral}
