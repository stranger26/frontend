import {RuleSetQuery} from "webpack";

let prodFileOpts: RuleSetQuery = {
	name: '[path][name].[hash].[ext]',
	publicPath: 'https://d2t8bm2bzw1p7l.cloudfront.net/',
	context: './src'
};

export {
	prodFileOpts
}
