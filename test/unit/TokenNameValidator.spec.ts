import {should} from 'chai';
import {Core, Kore} from "@kirinnee/core";
import {TokenNameValidator} from "../../src/classLibrary/TokenNameValidator";

const core: Core = new Kore();
core.ExtendPrimitives();

should();

describe("TokeNameValidator", () => {
	
	const validator: TokenNameValidator = new TokenNameValidator(core);
	
	describe("StartsWithAlphabet", () => {
		it("should return true if string starts with number", () => {
			validator.StartsWithAlphabet('Kirinnee').should.be.true;
			validator.StartsWithAlphabet('kirinnee').should.be.true;
			validator.StartsWithAlphabet('without1').should.be.true;
		});
		
		it("should return false if string starts with alphanumeric", () => {
			validator.StartsWithAlphabet('1irinnee').should.be.false;
			validator.StartsWithAlphabet('!irinnee').should.be.false;
			validator.StartsWithAlphabet(' ithout1').should.be.false;
		});
	});
	
	describe("Only contains alphanumeric _ and '-", () => {
		it('should return true if string contains only alphanumeric, _ and -', function () {
			validator.ContainOnlyAlphanumericUnderscoreDash('basic_token').should.be.true;
			validator.ContainOnlyAlphanumericUnderscoreDash('basic-token').should.be.true;
			validator.ContainOnlyAlphanumericUnderscoreDash('BasicToken').should.be.true;
			validator.ContainOnlyAlphanumericUnderscoreDash('Basic-Token32').should.be.true;
			validator.ContainOnlyAlphanumericUnderscoreDash('Basic_Token32').should.be.true;
			validator.ContainOnlyAlphanumericUnderscoreDash('Amazon_token-32').should.be.true;
		});
		
		it('should return false if string contains other characters', () => {
			validator.ContainOnlyAlphanumericUnderscoreDash('Basic-Token32!').should.be.false;
			validator.ContainOnlyAlphanumericUnderscoreDash('Basic:Token32').should.be.false;
			validator.ContainOnlyAlphanumericUnderscoreDash('Basic(Token32)').should.be.false;
			validator.ContainOnlyAlphanumericUnderscoreDash('B*e').should.be.false;
			validator.ContainOnlyAlphanumericUnderscoreDash('Basic Token').should.be.false;
		});
	});
	
	describe("Trimmed value should be less than 1", () => {
		it("should return true for string longer or equal to 1 character", () => {
			validator.AtLeastOneCharacter('kirinnee').should.be.true;
			validator.AtLeastOneCharacter(' k').should.be.true;
			validator.AtLeastOneCharacter('k ').should.be.true;
			
		});
		
		it("should return false for trimmed string shorted than 1 character", () => {
			validator.AtLeastOneCharacter('').should.be.false;
			validator.AtLeastOneCharacter(' ').should.be.false;
		});
	});
	
});
